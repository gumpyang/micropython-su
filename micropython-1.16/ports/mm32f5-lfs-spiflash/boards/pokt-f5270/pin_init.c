/* pin_init.c */
#include "pin_init.h"
#include "hal_rcc.h"
#include "hal_gpio.h"

void BOARD_InitPins(void)
{
    GPIO_Init_Type gpio_init;

    /* PB6 - UART1_TX. */
    gpio_init.Pins  = GPIO_PIN_6;
    gpio_init.PinMode  = GPIO_PinMode_AF_PushPull;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &gpio_init);
    GPIO_PinAFConf(GPIOB, gpio_init.Pins, GPIO_AF_7);

    /* PB7 - UART1_RX. */
    gpio_init.Pins  = GPIO_PIN_7;
    gpio_init.PinMode  = GPIO_PinMode_In_PullUp;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &gpio_init);
    GPIO_PinAFConf(GPIOB, gpio_init.Pins, GPIO_AF_7);

    /* for sdcard. */

    /* PC12 - GPIO output: SPI3_MOSI. */
    gpio_init.Pins  = GPIO_PIN_12;
    gpio_init.PinMode  = GPIO_PinMode_Out_PushPull;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &gpio_init);
    //GPIO_PinAFConf(GPIOC, gpio_init.Pins, GPIO_AF_6); /* gpio. disable af. */

    /* PC11 - GPIO input: SPI3_MISO. */
    gpio_init.Pins  = GPIO_PIN_11;
    gpio_init.PinMode  = GPIO_PinMode_In_PullUp;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &gpio_init);
    //GPIO_PinAFConf(GPIOC, gpio_init.Pins, GPIO_AF_6); /* gpio. disable af. */

    /* PC10 - GPIO output: SPI3_SCK. */
    gpio_init.Pins  = GPIO_PIN_10;
    gpio_init.PinMode  = GPIO_PinMode_Out_PushPull;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &gpio_init);
    //GPIO_PinAFConf(GPIOC, gpio_init.Pins, GPIO_AF_6); /* gpio. disable af. */

    /* PA15 - GPIO output: SPI3_CS. */
    gpio_init.Pins  = GPIO_PIN_15;
    gpio_init.PinMode  = GPIO_PinMode_Out_PushPull;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &gpio_init);
    //GPIO_PinAFConf(GPIOA, gpio_init.Pins, GPIO_AF_15); /* gpio, disable af. */

    /* for spiflash. */

    /* QSPI. */

    /* for pokt-f5270 with SPI mode simulated by GPIO. */
    /* PF6 - QSPI_CS. */    
    gpio_init.Pins  = GPIO_PIN_6;
    gpio_init.PinMode  = GPIO_PinMode_Out_PushPull; //GPIO output.
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOF, &gpio_init);
    GPIO_PinAFConf(GPIOF, gpio_init.Pins, GPIO_AF_15);
    GPIO_WriteBit(GPIOF, gpio_init.Pins, 1u); /* high voltage level.*/

    /* PG7 - QSPI_SCK. */
    gpio_init.Pins  = GPIO_PIN_7;
    gpio_init.PinMode  = GPIO_PinMode_Out_PushPull; //GPIO output.
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOG, &gpio_init);
    GPIO_PinAFConf(GPIOG, gpio_init.Pins, GPIO_AF_15);
    GPIO_WriteBit(GPIOG, gpio_init.Pins, 1u); /* high voltage level.*/

    /* PG6 - QSPI_IO0, DI. */
    gpio_init.Pins  = GPIO_PIN_6;
    gpio_init.PinMode  = GPIO_PinMode_Out_PushPull; //GPIO output.
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOG, &gpio_init);
    GPIO_PinAFConf(GPIOG, gpio_init.Pins, GPIO_AF_15);
    GPIO_WriteBit(GPIOG, gpio_init.Pins, 1u); /* high voltage level.*/

    /* PF8 - QSPI_IO1, DO. */
    gpio_init.Pins  = GPIO_PIN_8;
    gpio_init.PinMode  = GPIO_PinMode_In_Floating; //GPIO input.
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOF, &gpio_init);
    GPIO_PinAFConf(GPIOF, gpio_init.Pins, GPIO_AF_15);

    /* PF10 - QSPI_IO2. */
    gpio_init.Pins  = GPIO_PIN_10;
    gpio_init.PinMode  = GPIO_PinMode_Out_PushPull; //GPIO output.
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOF, &gpio_init);
    GPIO_PinAFConf(GPIOF, gpio_init.Pins, GPIO_AF_15);
    GPIO_WriteBit(GPIOF, gpio_init.Pins, 1u); /* high voltage level.*/

    /* PG8 - QSPI_IO3. */
    gpio_init.Pins  = GPIO_PIN_8;
    gpio_init.PinMode  = GPIO_PinMode_Out_PushPull; //GPIO_PinMode_AF_OpenDrain
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOG, &gpio_init);
    GPIO_PinAFConf(GPIOG, gpio_init.Pins, GPIO_AF_15);
    GPIO_WriteBit(GPIOG, gpio_init.Pins, 1u); /* high voltage level.*/

}


/* EOF. */

