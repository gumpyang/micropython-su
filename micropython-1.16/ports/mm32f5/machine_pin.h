/* machine_pin.h */

#ifndef __MACHINE_PIN_H__
#define __MACHINE_PIN_H__

#include "py/runtime.h"

#include "hal_gpio.h"

typedef enum
{
    PIN_MODE_IN_ANALOG = 0u,
    PIN_MODE_IN_FLOATING,
    PIN_MODE_IN_PULLDOWN,
    PIN_MODE_IN_PULLUP,
    PIN_MODE_OUT_OPENDRAIN,
    PIN_MODE_OUT_PUSHPULL,
    PIN_MODE_AF_OPENDRAIN,
    PIN_MODE_AF_PUSHPULL,
} machine_pin_mode_t;

/* Pin class instance configuration structure. */
typedef struct
{
    mp_obj_base_t base;      // object base class.
    qstr          name;      // pad name
    GPIO_Type   * gpio_port; // gpio instance for pin
    uint32_t      gpio_pin;  // pin number

} machine_pin_obj_t;

extern const machine_pin_obj_t * machine_pin_board_pins[];
extern const uint32_t            machine_pin_board_pins_num;
extern const mp_obj_dict_t       machine_pin_board_pins_locals_dict;

/* for all the module to bind the pins. */
const machine_pin_obj_t *pin_find(mp_obj_t user_obj);
const machine_pin_obj_t *pin_find_by_name(const mp_obj_dict_t *name_dict, mp_obj_t name);

extern const mp_obj_type_t machine_pin_type;

void machine_pin_set_mode(const machine_pin_obj_t *self, uint8_t mode);

#endif /* __MACHINE_PIN_H__ */

