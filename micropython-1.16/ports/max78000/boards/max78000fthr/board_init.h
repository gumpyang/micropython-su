#ifndef __BOARD_INIT_H__
#define __BOARD_INIT_H__

#include <stdio.h>
#include <stdint.h>

#include "hal_common.h"
#include "hal_gpio.h"
#include "hal_uart.h"
#include "hal_icc.h"
#include "hal_clock.h"

#include "clock_init.h"
#include "pin_init.h"

/* uart for debug console. */
#define BOARD_DEBUG_UART_PORT      UART0
#define BOARD_DEBUG_UART_BAUDRATE  9600u

void BOARD_Init(void);

void BOARD_InitDebugConsole(void);

#endif /* __BOARD_INIT_H__ */

