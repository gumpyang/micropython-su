/* mphalport.h */

#ifndef __mphalport_h__
#define __mphalport_h__

#include <stdint.h>
#include "machine_pin.h"

#define MP_HAL_PIN_FMT          "%q"
#define mp_hal_pin_obj_t        const machine_pin_obj_t *
#define mp_hal_get_pin_obj(o)   pin_find(o)
#define mp_hal_pin_name(p)      ((p)->name)

/* for virtual pin: SoftSPI . */
#define mp_hal_pin_write(p, value) GPIO_PinWrite(p->gpio_port, p->gpio_pin, value)
#define mp_hal_pin_read(p)         GPIO_PinRead(p->gpio_port, p->gpio_pin)
#define mp_hal_pin_output(p)       machine_pin_set_mode(p, PIN_MODE_OUT_PUSHPULL)
#define mp_hal_pin_input(p)        machine_pin_set_mode(p, PIN_MODE_IN_PULLUP)
#define mp_hal_pin_high(p)         GPIO_PinWrite(p->gpio_port, p->gpio_pin, 1u)
#define mp_hal_pin_low(p)          GPIO_PinWrite(p->gpio_port, p->gpio_pin, 0u)

/* for soft i2c. */
#define mp_hal_pin_open_drain(p)   machine_pin_set_mode(p, PIN_MODE_OUT_OPENDRAIN)
#define mp_hal_pin_od_low(p)       machine_pin_dir_output(p)
#define mp_hal_pin_od_high(p)      machine_pin_dir_input(p)

extern volatile uint32_t systick_ms;

/* 返回当前ms计数值。 */
static inline mp_uint_t mp_hal_ticks_ms(void)
{
    return systick_ms;
}

/* 返回当前us计数值。 */
static inline mp_uint_t mp_hal_ticks_us(void)
{
    return systick_ms * 1000;
}

/* 返回当前定时器计数值。 */
static inline mp_uint_t mp_hal_ticks_cpu(void)
{
    return systick_ms;
}

/* 延时us. */
//#define mp_hal_delay_us_fast(us) mp_hal_delay_us(us)
static inline void mp_hal_delay_us_fast(mp_uint_t us)
{
    for (uint32_t i = us; i > 0u; i--)
    {
        ;
    }
}

static inline void mp_hal_set_interrupt_char(char c) { }
//void mp_hal_set_interrupt_char(int c);

#endif /* __mphalport_h__ */
