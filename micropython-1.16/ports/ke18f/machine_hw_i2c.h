/* machine_hw_i2c.h */

#ifndef __MACHINE_HW_I2C_H__
#define __MACHINE_HW_I2C_H__

#include "py/runtime.h"
#include "py/mphal.h"
#include "py/mperrno.h"
#include "extmod/machine_i2c.h"
#include "machine_pin.h"

#include "fsl_lpi2c.h"
#include "fsl_gpio.h"
#include "fsl_port.h"
#include "fsl_clock.h"

typedef struct
{
    uint32_t freq;
    uint32_t timeout;
} machine_hw_i2c_conf_t;

typedef struct
{
    mp_obj_base_t base;      // object base class.

    const machine_pin_obj_t * sda_pin_obj;
    uint32_t sda_pin_af;

    const machine_pin_obj_t * scl_pin_obj;
    uint32_t scl_pin_af;

    LPI2C_Type * i2c_port;
    uint32_t     i2c_id;

    machine_hw_i2c_conf_t * conf;
} machine_hw_i2c_obj_t;

extern const machine_hw_i2c_obj_t * machine_hw_i2c_objs[];
extern const uint32_t               machine_hw_i2c_num;
extern const mp_obj_type_t          machine_hw_i2c_type;
const machine_hw_i2c_obj_t *hw_i2c_find(mp_obj_t user_obj);

#endif /* __MACHINE_HW_I2C_H__ */

/* EOF. */

