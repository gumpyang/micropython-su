/*
 * Copyright 2021 MindMotion Microelectronics Co., Ltd.
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __BOARD_INIT_H__
#define __BOARD_INIT_H__

#include <stdio.h>
#include <stdint.h>

#include "fsl_common.h"

#include "clock_config.h"
#include "pin_mux.h"

#include "fsl_port.h"
#include "fsl_gpio.h"
#include "fsl_lpuart.h"
#include "fsl_clock.h"

/* DEBUG UART. */
#define BOARD_DEBUG_UART_PORT        LPUART0
#define BOARD_DEBUG_UART_BAUDRATE    115200u
#define BOARD_DEBUG_UART_FREQ        CLOCK_GetIpFreq(kCLOCK_Lpuart0)

void BOARD_Init(void);

void BOARD_InitDebugConsole(void);

#endif /* __BOARD_INIT_H__ */

