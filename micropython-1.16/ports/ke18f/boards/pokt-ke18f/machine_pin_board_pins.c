/* machine_pin_board_pins. */

#include "fsl_common.h"
#include "machine_pin.h"
//#include "machine_dac.h"
//#include "machine_adc.h"
#include "machine_uart.h"
//#include "machine_spi.h"
#include "machine_hw_i2c.h"
//#include "machine_pwm.h"
//#include "machine_timer.h"

extern const mp_obj_type_t machine_pin_type;

const uint32_t machine_pin_board_pins_num = 100;

const machine_pin_obj_t pin_PE16 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE16, .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 16 };
const machine_pin_obj_t pin_PE15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE15, .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 15 };
const machine_pin_obj_t pin_PD1  = { .base = { &machine_pin_type }, .name = MP_QSTR_PD1 , .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 1  };
const machine_pin_obj_t pin_PD0  = { .base = { &machine_pin_type }, .name = MP_QSTR_PD0 , .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 0  };
const machine_pin_obj_t pin_PE11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE11, .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 11 };
const machine_pin_obj_t pin_PE10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE10, .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 10 };
const machine_pin_obj_t pin_PE13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE13, .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 13 };
const machine_pin_obj_t pin_PE5  = { .base = { &machine_pin_type }, .name = MP_QSTR_PE5 , .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 5  };
const machine_pin_obj_t pin_PE4  = { .base = { &machine_pin_type }, .name = MP_QSTR_PE4 , .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 4  };
const machine_pin_obj_t pin_PB7  = { .base = { &machine_pin_type }, .name = MP_QSTR_PB7 , .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 7  };
const machine_pin_obj_t pin_PB6  = { .base = { &machine_pin_type }, .name = MP_QSTR_PB6 , .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 6  };
const machine_pin_obj_t pin_PE14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE14, .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 14 };
const machine_pin_obj_t pin_PE3  = { .base = { &machine_pin_type }, .name = MP_QSTR_PE3 , .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 3  };
const machine_pin_obj_t pin_PE12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE12, .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 12 };
const machine_pin_obj_t pin_PD17 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD17, .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 17 };
const machine_pin_obj_t pin_PD16 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD16, .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 16 };
const machine_pin_obj_t pin_PD15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD15, .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 15 };
const machine_pin_obj_t pin_PE9  = { .base = { &machine_pin_type }, .name = MP_QSTR_PE9 , .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 9  };
const machine_pin_obj_t pin_PD14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD14, .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 14 };
const machine_pin_obj_t pin_PD13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD13, .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 13 };
const machine_pin_obj_t pin_PE8  = { .base = { &machine_pin_type }, .name = MP_QSTR_PE8 , .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 8  };
const machine_pin_obj_t pin_PB5  = { .base = { &machine_pin_type }, .name = MP_QSTR_PB5 , .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 5  };
const machine_pin_obj_t pin_PB4  = { .base = { &machine_pin_type }, .name = MP_QSTR_PB4 , .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 4  };
const machine_pin_obj_t pin_PC3  = { .base = { &machine_pin_type }, .name = MP_QSTR_PC3 , .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 3  };
const machine_pin_obj_t pin_PC2  = { .base = { &machine_pin_type }, .name = MP_QSTR_PC2 , .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 2  };
const machine_pin_obj_t pin_PD7  = { .base = { &machine_pin_type }, .name = MP_QSTR_PD7 , .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 7  };
const machine_pin_obj_t pin_PD6  = { .base = { &machine_pin_type }, .name = MP_QSTR_PD6 , .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 6  };
const machine_pin_obj_t pin_PD5  = { .base = { &machine_pin_type }, .name = MP_QSTR_PD5 , .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 5  };
const machine_pin_obj_t pin_PD12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD12, .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 12 };
const machine_pin_obj_t pin_PD11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD11, .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 11 };
const machine_pin_obj_t pin_PD10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD10, .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 10 };
const machine_pin_obj_t pin_PC1  = { .base = { &machine_pin_type }, .name = MP_QSTR_PC1 , .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 1  };
const machine_pin_obj_t pin_PC0  = { .base = { &machine_pin_type }, .name = MP_QSTR_PC0 , .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 0  };
const machine_pin_obj_t pin_PD9  = { .base = { &machine_pin_type }, .name = MP_QSTR_PD9 , .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 9  };
const machine_pin_obj_t pin_PD8  = { .base = { &machine_pin_type }, .name = MP_QSTR_PD8 , .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 8  };
const machine_pin_obj_t pin_PC17 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC17, .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 17 };
const machine_pin_obj_t pin_PC16 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC16, .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 16 };
const machine_pin_obj_t pin_PC15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC15, .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 15 };
const machine_pin_obj_t pin_PC14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC14, .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 14 };
const machine_pin_obj_t pin_PB3  = { .base = { &machine_pin_type }, .name = MP_QSTR_PB3 , .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 3  };
const machine_pin_obj_t pin_PB2  = { .base = { &machine_pin_type }, .name = MP_QSTR_PB2 , .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 2  };
const machine_pin_obj_t pin_PC13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC13, .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 13 };
const machine_pin_obj_t pin_PC12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC12, .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 12 };
const machine_pin_obj_t pin_PC11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC11, .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 11 };
const machine_pin_obj_t pin_PC10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC10, .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 10 };
const machine_pin_obj_t pin_PB1  = { .base = { &machine_pin_type }, .name = MP_QSTR_PB1 , .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 1  };
const machine_pin_obj_t pin_PB0  = { .base = { &machine_pin_type }, .name = MP_QSTR_PB0 , .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 0  };
const machine_pin_obj_t pin_PC9  = { .base = { &machine_pin_type }, .name = MP_QSTR_PC9 , .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 9  };
const machine_pin_obj_t pin_PC8  = { .base = { &machine_pin_type }, .name = MP_QSTR_PC8 , .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 8  };
const machine_pin_obj_t pin_PA7  = { .base = { &machine_pin_type }, .name = MP_QSTR_PA7 , .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 7  };
const machine_pin_obj_t pin_PA6  = { .base = { &machine_pin_type }, .name = MP_QSTR_PA6 , .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 6  };
const machine_pin_obj_t pin_PE7  = { .base = { &machine_pin_type }, .name = MP_QSTR_PE7 , .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 7  };
const machine_pin_obj_t pin_PA17 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA17, .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 17 };
const machine_pin_obj_t pin_PB17 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB17, .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 17 };
const machine_pin_obj_t pin_PB16 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB16, .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 16 };
const machine_pin_obj_t pin_PB15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB15, .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 15 };
const machine_pin_obj_t pin_PB14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB14, .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 14 };
const machine_pin_obj_t pin_PB13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB13, .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 13 };
const machine_pin_obj_t pin_PB12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB12, .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 12 };
const machine_pin_obj_t pin_PD4  = { .base = { &machine_pin_type }, .name = MP_QSTR_PD4 , .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 4  };
const machine_pin_obj_t pin_PD3  = { .base = { &machine_pin_type }, .name = MP_QSTR_PD3 , .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 3  };
const machine_pin_obj_t pin_PD2  = { .base = { &machine_pin_type }, .name = MP_QSTR_PD2 , .io_port = PORTD, .gpio_port = GPIOD, .gpio_pin = 2  };
const machine_pin_obj_t pin_PA3  = { .base = { &machine_pin_type }, .name = MP_QSTR_PA3 , .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 3  };
const machine_pin_obj_t pin_PA2  = { .base = { &machine_pin_type }, .name = MP_QSTR_PA2 , .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 2  };
const machine_pin_obj_t pin_PB11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB11, .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 11 };
const machine_pin_obj_t pin_PB10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB10, .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 10 };
const machine_pin_obj_t pin_PB9  = { .base = { &machine_pin_type }, .name = MP_QSTR_PB9 , .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 9  };
const machine_pin_obj_t pin_PB8  = { .base = { &machine_pin_type }, .name = MP_QSTR_PB8 , .io_port = PORTB, .gpio_port = GPIOB, .gpio_pin = 8  };
const machine_pin_obj_t pin_PA1  = { .base = { &machine_pin_type }, .name = MP_QSTR_PA1 , .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 1  };
const machine_pin_obj_t pin_PA0  = { .base = { &machine_pin_type }, .name = MP_QSTR_PA0 , .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 0  };
const machine_pin_obj_t pin_PC7  = { .base = { &machine_pin_type }, .name = MP_QSTR_PC7 , .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 7  };
const machine_pin_obj_t pin_PC6  = { .base = { &machine_pin_type }, .name = MP_QSTR_PC6 , .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 6  };
const machine_pin_obj_t pin_PA16 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA16, .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 16 };
const machine_pin_obj_t pin_PA15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA15, .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 15 };
const machine_pin_obj_t pin_PE6  = { .base = { &machine_pin_type }, .name = MP_QSTR_PE6 , .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 6  };
const machine_pin_obj_t pin_PE2  = { .base = { &machine_pin_type }, .name = MP_QSTR_PE2 , .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 2  };
const machine_pin_obj_t pin_PA13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA13, .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 13 };
const machine_pin_obj_t pin_PA12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA12, .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 12 };
const machine_pin_obj_t pin_PA11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA11, .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 11 };
const machine_pin_obj_t pin_PA10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA10, .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 10 };
const machine_pin_obj_t pin_PE1  = { .base = { &machine_pin_type }, .name = MP_QSTR_PE1 , .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 1  };
const machine_pin_obj_t pin_PE0  = { .base = { &machine_pin_type }, .name = MP_QSTR_PE0 , .io_port = PORTE, .gpio_port = GPIOE, .gpio_pin = 0  };
const machine_pin_obj_t pin_PC5  = { .base = { &machine_pin_type }, .name = MP_QSTR_PC5 , .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 5  };
const machine_pin_obj_t pin_PC4  = { .base = { &machine_pin_type }, .name = MP_QSTR_PC4 , .io_port = PORTC, .gpio_port = GPIOC, .gpio_pin = 4  };
const machine_pin_obj_t pin_PA5  = { .base = { &machine_pin_type }, .name = MP_QSTR_PA5 , .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 5  };
const machine_pin_obj_t pin_PA4  = { .base = { &machine_pin_type }, .name = MP_QSTR_PA4 , .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 4  };
const machine_pin_obj_t pin_PA9  = { .base = { &machine_pin_type }, .name = MP_QSTR_PA9 , .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 9  };
const machine_pin_obj_t pin_PA8  = { .base = { &machine_pin_type }, .name = MP_QSTR_PA8 , .io_port = PORTA, .gpio_port = GPIOA, .gpio_pin = 8  };

/* pin id in the package. */
const machine_pin_obj_t* machine_pin_board_pins[] =
{
    &pin_PE16,
    &pin_PE15,
    &pin_PD1 ,
    &pin_PD0 ,
    &pin_PE11,
    &pin_PE10,
    &pin_PE13,
    &pin_PE5 ,
    &pin_PE4 ,
    NULL, /* VDD. */
    NULL, /* VDDA. */
    NULL, /* VREFH */
    NULL, /* VREFL. */
    NULL, /* VSS. */
    &pin_PB7 ,
    &pin_PB6 ,
    &pin_PE14,
    &pin_PE3 ,
    &pin_PE12,
    &pin_PD17,
    &pin_PD16,
    &pin_PD15,
    &pin_PE9 ,
    &pin_PD14,
    &pin_PD13,
    &pin_PE8 ,
    &pin_PB5 ,
    &pin_PB4 ,
    &pin_PC3 ,
    &pin_PC2 ,
    &pin_PD7 ,
    &pin_PD6 ,
    &pin_PD5 ,
    &pin_PD12,
    &pin_PD11,
    &pin_PD10,
    NULL, /* VSS. */
    NULL, /* VDD. */
    &pin_PC1 ,
    &pin_PC0 ,
    &pin_PD9 ,
    &pin_PD8 ,
    &pin_PC17,
    &pin_PC16,
    &pin_PC15,
    &pin_PC14,
    &pin_PB3 ,
    &pin_PB2 ,
    &pin_PC13,
    &pin_PC12,
    &pin_PC11,
    &pin_PC10,
    &pin_PB1 ,
    &pin_PB0 ,
    &pin_PC9 ,
    &pin_PC8 ,
    &pin_PA7 ,
    &pin_PA6 ,
    &pin_PE7 ,
    NULL, /* VSS. */
    NULL, /* VDD. */
    &pin_PA17,
    &pin_PB17,
    &pin_PB16,
    &pin_PB15,
    &pin_PB14,
    &pin_PB13,
    &pin_PB12,
    &pin_PD4 ,
    &pin_PD3 ,
    &pin_PD2 ,
    &pin_PA3 ,
    &pin_PA2 ,
    &pin_PB11,
    &pin_PB10,
    &pin_PB9 ,
    &pin_PB8 ,
    &pin_PA1 ,
    &pin_PA0 ,
    &pin_PC7 ,
    &pin_PC6 ,
    &pin_PA16,
    &pin_PA15,
    &pin_PE6 ,
    &pin_PE2 ,
    NULL, /* VSS. */
    NULL, /* VDD. */
    &pin_PA13,
    &pin_PA12,
    &pin_PA11,
    &pin_PA10,
    &pin_PE1 ,
    &pin_PE0 ,
    &pin_PC5 ,
    &pin_PC4 ,
    &pin_PA5 ,
    &pin_PA4 ,
    &pin_PA9 ,
    &pin_PA8 ,
};

STATIC const mp_rom_map_elem_t machine_pin_board_pins_locals_dict_table[] =
{
    { MP_ROM_QSTR(MP_QSTR_PE16), MP_ROM_PTR(&pin_PE16 ) },
    { MP_ROM_QSTR(MP_QSTR_PE15), MP_ROM_PTR(&pin_PE15 ) },
    { MP_ROM_QSTR(MP_QSTR_PD1 ), MP_ROM_PTR(&pin_PD1  ) },
    { MP_ROM_QSTR(MP_QSTR_PD0 ), MP_ROM_PTR(&pin_PD0  ) },
    { MP_ROM_QSTR(MP_QSTR_PE11), MP_ROM_PTR(&pin_PE11 ) },
    { MP_ROM_QSTR(MP_QSTR_PE10), MP_ROM_PTR(&pin_PE10 ) },
    { MP_ROM_QSTR(MP_QSTR_PE13), MP_ROM_PTR(&pin_PE13 ) },
    { MP_ROM_QSTR(MP_QSTR_PE5 ), MP_ROM_PTR(&pin_PE5  ) },
    { MP_ROM_QSTR(MP_QSTR_PE4 ), MP_ROM_PTR(&pin_PE4  ) },
    { MP_ROM_QSTR(MP_QSTR_PB7 ), MP_ROM_PTR(&pin_PB7  ) },
    { MP_ROM_QSTR(MP_QSTR_PB6 ), MP_ROM_PTR(&pin_PB6  ) },
    { MP_ROM_QSTR(MP_QSTR_PE14), MP_ROM_PTR(&pin_PE14 ) },
    { MP_ROM_QSTR(MP_QSTR_PE3 ), MP_ROM_PTR(&pin_PE3  ) },
    { MP_ROM_QSTR(MP_QSTR_PE12), MP_ROM_PTR(&pin_PE12 ) },
    { MP_ROM_QSTR(MP_QSTR_PD17), MP_ROM_PTR(&pin_PD17 ) },
    { MP_ROM_QSTR(MP_QSTR_PD16), MP_ROM_PTR(&pin_PD16 ) },
    { MP_ROM_QSTR(MP_QSTR_PD15), MP_ROM_PTR(&pin_PD15 ) },
    { MP_ROM_QSTR(MP_QSTR_PE9 ), MP_ROM_PTR(&pin_PE9  ) },
    { MP_ROM_QSTR(MP_QSTR_PD14), MP_ROM_PTR(&pin_PD14 ) },
    { MP_ROM_QSTR(MP_QSTR_PD13), MP_ROM_PTR(&pin_PD13 ) },
    { MP_ROM_QSTR(MP_QSTR_PE8 ), MP_ROM_PTR(&pin_PE8  ) },
    { MP_ROM_QSTR(MP_QSTR_PB5 ), MP_ROM_PTR(&pin_PB5  ) },
    { MP_ROM_QSTR(MP_QSTR_PB4 ), MP_ROM_PTR(&pin_PB4  ) },
    { MP_ROM_QSTR(MP_QSTR_PC3 ), MP_ROM_PTR(&pin_PC3  ) },
    { MP_ROM_QSTR(MP_QSTR_PC2 ), MP_ROM_PTR(&pin_PC2  ) },
    { MP_ROM_QSTR(MP_QSTR_PD7 ), MP_ROM_PTR(&pin_PD7  ) },
    { MP_ROM_QSTR(MP_QSTR_PD6 ), MP_ROM_PTR(&pin_PD6  ) },
    { MP_ROM_QSTR(MP_QSTR_PD5 ), MP_ROM_PTR(&pin_PD5  ) },
    { MP_ROM_QSTR(MP_QSTR_PD12), MP_ROM_PTR(&pin_PD12 ) },
    { MP_ROM_QSTR(MP_QSTR_PD11), MP_ROM_PTR(&pin_PD11 ) },
    { MP_ROM_QSTR(MP_QSTR_PD10), MP_ROM_PTR(&pin_PD10 ) },
    { MP_ROM_QSTR(MP_QSTR_PC1 ), MP_ROM_PTR(&pin_PC1  ) },
    { MP_ROM_QSTR(MP_QSTR_PC0 ), MP_ROM_PTR(&pin_PC0  ) },
    { MP_ROM_QSTR(MP_QSTR_PD9 ), MP_ROM_PTR(&pin_PD9  ) },
    { MP_ROM_QSTR(MP_QSTR_PD8 ), MP_ROM_PTR(&pin_PD8  ) },
    { MP_ROM_QSTR(MP_QSTR_PC17), MP_ROM_PTR(&pin_PC17 ) },
    { MP_ROM_QSTR(MP_QSTR_PC16), MP_ROM_PTR(&pin_PC16 ) },
    { MP_ROM_QSTR(MP_QSTR_PC15), MP_ROM_PTR(&pin_PC15 ) },
    { MP_ROM_QSTR(MP_QSTR_PC14), MP_ROM_PTR(&pin_PC14 ) },
    { MP_ROM_QSTR(MP_QSTR_PB3 ), MP_ROM_PTR(&pin_PB3  ) },
    { MP_ROM_QSTR(MP_QSTR_PB2 ), MP_ROM_PTR(&pin_PB2  ) },
    { MP_ROM_QSTR(MP_QSTR_PC13), MP_ROM_PTR(&pin_PC13 ) },
    { MP_ROM_QSTR(MP_QSTR_PC12), MP_ROM_PTR(&pin_PC12 ) },
    { MP_ROM_QSTR(MP_QSTR_PC11), MP_ROM_PTR(&pin_PC11 ) },
    { MP_ROM_QSTR(MP_QSTR_PC10), MP_ROM_PTR(&pin_PC10 ) },
    { MP_ROM_QSTR(MP_QSTR_PB1 ), MP_ROM_PTR(&pin_PB1  ) },
    { MP_ROM_QSTR(MP_QSTR_PB0 ), MP_ROM_PTR(&pin_PB0  ) },
    { MP_ROM_QSTR(MP_QSTR_PC9 ), MP_ROM_PTR(&pin_PC9  ) },
    { MP_ROM_QSTR(MP_QSTR_PC8 ), MP_ROM_PTR(&pin_PC8  ) },
    { MP_ROM_QSTR(MP_QSTR_PA7 ), MP_ROM_PTR(&pin_PA7  ) },
    { MP_ROM_QSTR(MP_QSTR_PA6 ), MP_ROM_PTR(&pin_PA6  ) },
    { MP_ROM_QSTR(MP_QSTR_PE7 ), MP_ROM_PTR(&pin_PE7  ) },
    { MP_ROM_QSTR(MP_QSTR_PA17), MP_ROM_PTR(&pin_PA17 ) },
    { MP_ROM_QSTR(MP_QSTR_PB17), MP_ROM_PTR(&pin_PB17 ) },
    { MP_ROM_QSTR(MP_QSTR_PB16), MP_ROM_PTR(&pin_PB16 ) },
    { MP_ROM_QSTR(MP_QSTR_PB15), MP_ROM_PTR(&pin_PB15 ) },
    { MP_ROM_QSTR(MP_QSTR_PB14), MP_ROM_PTR(&pin_PB14 ) },
    { MP_ROM_QSTR(MP_QSTR_PB13), MP_ROM_PTR(&pin_PB13 ) },
    { MP_ROM_QSTR(MP_QSTR_PB12), MP_ROM_PTR(&pin_PB12 ) },
    { MP_ROM_QSTR(MP_QSTR_PD4 ), MP_ROM_PTR(&pin_PD4  ) },
    { MP_ROM_QSTR(MP_QSTR_PD3 ), MP_ROM_PTR(&pin_PD3  ) },
    { MP_ROM_QSTR(MP_QSTR_PD2 ), MP_ROM_PTR(&pin_PD2  ) },
    { MP_ROM_QSTR(MP_QSTR_PA3 ), MP_ROM_PTR(&pin_PA3  ) },
    { MP_ROM_QSTR(MP_QSTR_PA2 ), MP_ROM_PTR(&pin_PA2  ) },
    { MP_ROM_QSTR(MP_QSTR_PB11), MP_ROM_PTR(&pin_PB11 ) },
    { MP_ROM_QSTR(MP_QSTR_PB10), MP_ROM_PTR(&pin_PB10 ) },
    { MP_ROM_QSTR(MP_QSTR_PB9 ), MP_ROM_PTR(&pin_PB9  ) },
    { MP_ROM_QSTR(MP_QSTR_PB8 ), MP_ROM_PTR(&pin_PB8  ) },
    { MP_ROM_QSTR(MP_QSTR_PA1 ), MP_ROM_PTR(&pin_PA1  ) },
    { MP_ROM_QSTR(MP_QSTR_PA0 ), MP_ROM_PTR(&pin_PA0  ) },
    { MP_ROM_QSTR(MP_QSTR_PC7 ), MP_ROM_PTR(&pin_PC7  ) },
    { MP_ROM_QSTR(MP_QSTR_PC6 ), MP_ROM_PTR(&pin_PC6  ) },
    { MP_ROM_QSTR(MP_QSTR_PA16), MP_ROM_PTR(&pin_PA16 ) },
    { MP_ROM_QSTR(MP_QSTR_PA15), MP_ROM_PTR(&pin_PA15 ) },
    { MP_ROM_QSTR(MP_QSTR_PE6 ), MP_ROM_PTR(&pin_PE6  ) },
    { MP_ROM_QSTR(MP_QSTR_PE2 ), MP_ROM_PTR(&pin_PE2  ) },
    { MP_ROM_QSTR(MP_QSTR_PA13), MP_ROM_PTR(&pin_PA13 ) },
    { MP_ROM_QSTR(MP_QSTR_PA12), MP_ROM_PTR(&pin_PA12 ) },
    { MP_ROM_QSTR(MP_QSTR_PA11), MP_ROM_PTR(&pin_PA11 ) },
    { MP_ROM_QSTR(MP_QSTR_PA10), MP_ROM_PTR(&pin_PA10 ) },
    { MP_ROM_QSTR(MP_QSTR_PE1 ), MP_ROM_PTR(&pin_PE1  ) },
    { MP_ROM_QSTR(MP_QSTR_PE0 ), MP_ROM_PTR(&pin_PE0  ) },
    { MP_ROM_QSTR(MP_QSTR_PC5 ), MP_ROM_PTR(&pin_PC5  ) },
    { MP_ROM_QSTR(MP_QSTR_PC4 ), MP_ROM_PTR(&pin_PC4  ) },
    { MP_ROM_QSTR(MP_QSTR_PA5 ), MP_ROM_PTR(&pin_PA5  ) },
    { MP_ROM_QSTR(MP_QSTR_PA4 ), MP_ROM_PTR(&pin_PA4  ) },
    { MP_ROM_QSTR(MP_QSTR_PA9 ), MP_ROM_PTR(&pin_PA9  ) },
    { MP_ROM_QSTR(MP_QSTR_PA8 ), MP_ROM_PTR(&pin_PA8  ) },
};

MP_DEFINE_CONST_DICT(machine_pin_board_pins_locals_dict, machine_pin_board_pins_locals_dict_table);

/* UART. */
const uint32_t machine_uart_num = 3u;

machine_uart_conf_t machine_uart_conf[3]; /* static mamory instead of malloc(). */
LPUART_Type * const machine_uart_port[3] = {LPUART0, LPUART1, LPUART2};

const machine_uart_obj_t uart_0 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PB0, .rx_pin_af = 2, .tx_pin_obj = &pin_PB1, .tx_pin_af = 2, .uart_port = machine_uart_port[0], .uart_irqn = LPUART0_RX_IRQn, .uart_id = 0u, .conf = &machine_uart_conf[0]};
const machine_uart_obj_t uart_1 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PC8, .rx_pin_af = 2, .tx_pin_obj = &pin_PC9, .tx_pin_af = 2, .uart_port = machine_uart_port[1], .uart_irqn = LPUART1_RX_IRQn, .uart_id = 1u, .conf = &machine_uart_conf[1]};
const machine_uart_obj_t uart_2 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PD6, .rx_pin_af = 2, .tx_pin_obj = &pin_PD7, .tx_pin_af = 2, .uart_port = machine_uart_port[2], .uart_irqn = LPUART2_RX_IRQn, .uart_id = 2u, .conf = &machine_uart_conf[2]};

const machine_uart_obj_t * machine_uart_objs[] =
{
    &uart_0,
    &uart_1,
    &uart_2,
};

/* I2C. */
const uint32_t machine_hw_i2c_num = 1u;

machine_hw_i2c_conf_t machine_hw_i2c_conf[1]; /* static mamory instead of malloc(). */
LPI2C_Type * const machine_hw_i2c_port[1] = {LPI2C0};

const machine_hw_i2c_obj_t hw_i2c_0 = { .base = { &machine_hw_i2c_type }, .sda_pin_obj = &pin_PA2, .sda_pin_af = 3, .scl_pin_obj = &pin_PA3, .scl_pin_af = 3, .i2c_port = machine_hw_i2c_port[0], .i2c_id = 0u, .conf = &machine_hw_i2c_conf[0]};

const machine_hw_i2c_obj_t * machine_hw_i2c_objs[] =
{
    &hw_i2c_0,
};

/* EOF. */

