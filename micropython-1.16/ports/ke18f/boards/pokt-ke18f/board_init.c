/*
 * Copyright 2021 MindMotion Microelectronics Co., Ltd.
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "board_init.h"

/*
* Definitions.
*/

/*
* Declerations.
*/
/* 系统时基定时器计数值。 */
volatile uint32_t systick_ms = 0u;

//void BOARD_InitDebugConsole(void);

/*
* Functions.
*/
void BOARD_Init(void)
{
    BOARD_InitBootClocks();
    CLOCK_EnableClock(kCLOCK_PortA);
    CLOCK_EnableClock(kCLOCK_PortB);
    CLOCK_EnableClock(kCLOCK_PortC);
    CLOCK_EnableClock(kCLOCK_PortD);
    CLOCK_EnableClock(kCLOCK_PortE);

    BOARD_InitBootPins();
    BOARD_InitDebugConsole();

    /* Enable Systick. */
    SysTick_Config(BOARD_BOOTCLOCKRUN_CORE_CLOCK / 1000u); /* 1000. */
}

void BOARD_InitDebugConsole(void)
{
    lpuart_config_t lpuart_config =
    {
        .baudRate_Bps = BOARD_DEBUG_UART_BAUDRATE,
        .parityMode = kLPUART_ParityDisabled,
        .dataBitsCount = kLPUART_EightDataBits,
        .isMsb = false,
        .stopBitCount = kLPUART_OneStopBit,
        .txFifoWatermark = 0U,
        .rxFifoWatermark = 1U,
        .enableRxRTS = false,
        .enableTxCTS = false,
        .txCtsSource = kLPUART_CtsSourcePin,
        .txCtsConfig = kLPUART_CtsSampleAtStart,
        .rxIdleType = kLPUART_IdleTypeStartBit,
        .rxIdleConfig = kLPUART_IdleCharacter1,
        .enableTx = true,
        .enableRx = true
    };

    LPUART_Init(BOARD_DEBUG_UART_PORT, &lpuart_config, BOARD_DEBUG_UART_FREQ);
}

/* EOF. */
