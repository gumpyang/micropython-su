/* sdcard_spi_port.c */
#include "fsl_common.h"
#include "fsl_clock.h"
#include "fsl_gpio.h"

#include "sdspi.h"

#define BOARD_SDSPI_TX_GPIO_PORT GPIOE
#define BOARD_SDSPI_TX_GPIO_PIN  11u

#define BOARD_SDSPI_RX_GPIO_PORT GPIOE
#define BOARD_SDSPI_RX_GPIO_PIN  5u

#define BOARD_SDSPI_CLK_GPIO_PORT GPIOE
#define BOARD_SDSPI_CLK_GPIO_PIN  4u

#define BOARD_SDSPI_CS_GPIO_PORT GPIOE
#define BOARD_SDSPI_CS_GPIO_PIN  10u

uint32_t board_sdspi_delay_count;

SDSPI_ApiRetStatus_Type sdspi_spi_init(void);
SDSPI_ApiRetStatus_Type sdspi_spi_freq(uint32_t hz);
SDSPI_ApiRetStatus_Type sdspi_spi_xfer(uint8_t *in, uint8_t *out, uint32_t size);

const SDSPI_Interface_Type board_sdspi_if =
{
    .baudrate = 1000000u, /* 1mhz. */
    .spi_init = sdspi_spi_init,
    .spi_freq = sdspi_spi_freq,
    .spi_xfer = sdspi_spi_xfer
};

static void board_sdspi_delay(uint32_t count)
{
    for (uint32_t i = count; i > 0u; i--)
    {
        __NOP();
    }
}

/* pins:
 * tx : PTE11/SPI0_MOSI
 * rx : PTE5 /SPI0_MISO
 * clk: PTE4 /SPI0_SCK
 * cs : PTE10/SPI0_PCS0
 */

SDSPI_ApiRetStatus_Type sdspi_spi_init(void)
{
    gpio_pin_config_t gpio_pin_config;
    gpio_pin_config.pinDirection = kGPIO_DigitalOutput;
    gpio_pin_config.outputLogic = 1u;
    GPIO_PinInit(BOARD_SDSPI_CS_GPIO_PORT , BOARD_SDSPI_CS_GPIO_PIN , &gpio_pin_config);
    
    gpio_pin_config.outputLogic = 0u;
    GPIO_PinInit(BOARD_SDSPI_TX_GPIO_PORT , BOARD_SDSPI_TX_GPIO_PIN , &gpio_pin_config);
    GPIO_PinInit(BOARD_SDSPI_CLK_GPIO_PORT, BOARD_SDSPI_CLK_GPIO_PIN, &gpio_pin_config);
    
    gpio_pin_config.pinDirection = kGPIO_DigitalInput;
    GPIO_PinInit(BOARD_SDSPI_RX_GPIO_PORT , BOARD_SDSPI_RX_GPIO_PIN , &gpio_pin_config);
    
    board_sdspi_delay_count = 100u;
    
    
#if 0
    /* pin mux. */
    CLOCK_EnableClock(kCLOCK_PortC);
    PORTC->PCR[4] = PORT_PCR_MUX(1); /* PTC4: SPI0_PCS0, GPIO模式. */
    PORTC->PCR[5] = PORT_PCR_MUX(2); /* PTC5: SPI0_CLK. */
    PORTC->PCR[6] = PORT_PCR_MUX(2); /* PTC6: SPI0_MOSI */
    PORTC->PCR[7] = PORT_PCR_MUX(2) | PORT_PCR_PE_MASK | PORT_PCR_PS_MASK; /* PTC7: SPI0_MISO. */

    /* gpio for cs pin. */
    gpio_pin_config_t gpio_pin_config;
    gpio_pin_config.pinDirection = kGPIO_DigitalOutput;
    gpio_pin_config.outputLogic = 1u;
    GPIO_PinInit(GPIOC, 4u, &gpio_pin_config);

    /* spi0. */
    /* spi0使用bus clock. */
    CLOCK_EnableClock(kCLOCK_Spi0);
    SPI0->C1 = SPI_C1_SPE_MASK /* enable spi. */
             | SPI_C1_MSTR_MASK /* master mode. */
             | SPI_C1_CPOL(0) /* clock polarity. */
             | SPI_C1_CPHA(0) /* phase polarity. */
             ;
    SPI0->C2 = 0u;
    SPI0->BR = SPI_BR_SPPR(2) | SPI_BR_SPR(2); /* div = 3x8. busclk = 24mhz, baudrate = 1mhz. */
#endif
    return kStatus_Success;
}

SDSPI_ApiRetStatus_Type sdspi_spi_freq(uint32_t hz)
{
    switch (hz)
    {
    case SDMMC_CLOCK_400KHZ:
        //SPI0->BR = SPI_BR_SPPR(2) | SPI_BR_SPR(4); /* div = 3x32. busclk = 24mhz, baudrate = 250khz. */
        board_sdspi_delay_count = 1000u;
        break;
    default:
        //SPI0->BR = SPI_BR_SPPR(2) | SPI_BR_SPR(2); /* div = 3x32. busclk = 24mhz, baudrate = 1mhz. */
        board_sdspi_delay_count = 100u;
        break;
    }
    return kStatus_Success;
}

uint8_t spi_xfer(uint8_t tx_dat)
{
    uint8_t rx_dat = 0u;
#if 0
    while (! (SPI0->S & SPI_S_SPTEF_MASK) )
    {}
    SPI0->DL = tx_dat;
    while (! (SPI0->S & SPI_S_SPRF_MASK) )
    {}
    rx_dat = SPI0->DL;
#endif
    
    for (uint8_t i = 0u; i < 8u; i++)
    {
        GPIO_PinWrite(BOARD_SDSPI_CLK_GPIO_PORT, BOARD_SDSPI_CLK_GPIO_PIN, 0u );
        GPIO_PinWrite(BOARD_SDSPI_TX_GPIO_PORT, BOARD_SDSPI_TX_GPIO_PIN, (0u != (tx_dat & (1u << (7u-i)))) ? 1u : 0u );
        board_sdspi_delay(board_sdspi_delay_count);
        GPIO_PinWrite(BOARD_SDSPI_CLK_GPIO_PORT, BOARD_SDSPI_CLK_GPIO_PIN, 1u );
        board_sdspi_delay(board_sdspi_delay_count);
        rx_dat = (rx_dat << 1u) | GPIO_PinRead(BOARD_SDSPI_RX_GPIO_PORT, BOARD_SDSPI_RX_GPIO_PIN);
        
    }

    return rx_dat;
}

void spi_assert_cs(bool enable)
{
    GPIO_PinWrite(BOARD_SDSPI_CS_GPIO_PORT, BOARD_SDSPI_CS_GPIO_PIN, (enable ? 0u: 1u) );
}

SDSPI_ApiRetStatus_Type sdspi_spi_xfer(uint8_t *in, uint8_t *out, uint32_t size)
{
    uint8_t inbuf, outbuf;
    spi_assert_cs(true);

    for (uint32_t i = 0u; i < size; i++)
    {
        inbuf = (in == NULL) ? SDSPI_DUMMY_DATA: *in++;
        outbuf = spi_xfer(inbuf);
        if (out)
        {
            *out = outbuf;
            out++;
        }
    }

    spi_assert_cs(false);

    return kStatus_Success;
}

/* EOF. */

