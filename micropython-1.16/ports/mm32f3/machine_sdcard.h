/* machine_sdcard.h */

#ifndef __MACHINE_SDCARD_H__
#define __MACHINE_SDCARD_H__

#include "py/runtime.h"
#include "extmod/vfs.h"
#include "extmod/vfs_fat.h"
#include "lib/oofatfs/ff.h"

extern const struct _mp_obj_type_t machine_sdcard_type;
extern const struct _mp_obj_base_t machine_sdcard_obj;

bool machine_sdcard_init(void);
void machine_sdcard_init_vfs(fs_user_mount_t *vfs);

#endif /* __MACHINE_SDCARD_H__ */


