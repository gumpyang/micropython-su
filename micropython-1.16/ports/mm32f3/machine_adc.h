/* machine_adc.h */

#ifndef __MACHINE_ADC_H__
#define __MACHINE_ADC_H__

#include "py/runtime.h"

#include "machine_pin.h"
#include "hal_adc.h"


/* ADC class instance configuration structure. */

typedef struct
{
    uint32_t active_channels;
} machine_adc_conf_t;

typedef struct
{
    mp_obj_base_t base;      // object base class.
    const machine_pin_obj_t * pin_obj;

    ADC_Type     * adc_port;
    uint32_t             adc_channel;

    machine_adc_conf_t * conf;

} machine_adc_obj_t;

extern const machine_adc_obj_t * machine_adc_objs[];
extern const uint32_t            machine_adc_port_num;
extern const uint32_t 			 machine_adc_channel_num_per_port ;
extern const uint32_t 			 machine_adc_channel_num;
extern       ADC_Type    * const machine_adc_port[];
extern const mp_obj_type_t       machine_adc_type;

const machine_adc_obj_t *adc_find(mp_obj_t user_obj);



#endif /* __MACHINE_PIN_H__ */

