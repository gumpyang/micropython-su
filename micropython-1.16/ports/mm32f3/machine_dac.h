/* machine_dac.h */

#ifndef __MACHINE_DAC_H__
#define __MACHINE_DAC_H__

#include "py/runtime.h"

#include "machine_pin.h"
#include "hal_dac.h"

typedef enum
{
    DAC_ALIGN_LEFT  = 0, /* pad the low bits. */
    DAC_ALIGN_RIGHT = 1, /* pad the hight bits. */
} machine_dac_align_t;

typedef struct
{
    machine_dac_align_t align;
    uint16_t value; /* current output value. */
} machine_dac_conf_t;

/* DAC class instance configuration structure. */
typedef struct
{
    mp_obj_base_t base; /* object base class. */

    /* bond to pin(s). */
    const machine_pin_obj_t * pin_obj;

    /* for hardware peripheral. */
    DAC_Type    * dac_port;
    uint32_t      dac_channel;

    /* for runtime settings. */
    machine_dac_conf_t *conf;
} machine_dac_obj_t;

extern const uint32_t machine_dac_num; /* import from machine_pin_board_pins.c. */
extern const machine_dac_obj_t * machine_dac_objs[];
extern const mp_obj_type_t machine_dac_type;

const machine_dac_obj_t *dac_find(mp_obj_t user_obj);

#endif /* __MACHINE_DAC_H__ */

