/* machine_i2c.h */

#ifndef __MACHINE_I2C_H__
#define __MACHINE_I2C_H__

#include "py/runtime.h"

#include "machine_pin.h"
#include "extmod/machine_i2c.h"
#include "hal_i2c.h"

#define MACHINE_HW_I2C_NUM            3u /* machine_hw_i2c_num. */

typedef struct
{
    uint32_t baudrate;
    uint32_t timeout;
}machine_hw_i2c_conf_t;

typedef struct
{
    mp_obj_base_t base;      // object base class.

    const machine_pin_obj_t * scl_pin_obj;
    uint32_t scl_pin_af;
    const machine_pin_obj_t * sda_pin_obj;
    uint32_t sda_pin_af;

    I2C_Type * i2c_port;
    uint32_t   i2c_id;

    machine_hw_i2c_conf_t * conf;
} machine_hw_i2c_obj_t;

extern const mp_obj_type_t          machine_hw_i2c_type;
extern const machine_hw_i2c_obj_t * machine_hw_i2c_objs[];

#endif /* __MACHINE_I2C_H__ */

