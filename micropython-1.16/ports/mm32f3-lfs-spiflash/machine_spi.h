/* machine_spi.h */

#ifndef __MACHINE_SPI_H__
#define __MACHINE_SPI_H__

#include "py/runtime.h"

#include "machine_pin.h"
#include "machine_spi.h"
#include "hal_spi.h"

#define MACHINE_HW_SPI_NUM            3u /* machine_uart_num. */

/* UART class instance configuration structure. */
typedef struct
{
    uint32_t baudrate;
    uint32_t polarity;
    uint32_t phase;
} machine_hw_spi_conf_t;

typedef struct
{
    mp_obj_base_t base;      // object base class.

    const machine_pin_obj_t * sck_pin_obj;
    uint32_t sck_pin_af;

    const machine_pin_obj_t * mosi_pin_obj;
    uint32_t mosi_pin_af;

    const machine_pin_obj_t * miso_pin_obj;
    uint32_t miso_pin_af;

    SPI_Type * spi_port;
    IRQn_Type  spi_irqn;
    uint32_t   spi_id;

    machine_hw_spi_conf_t * conf;
} machine_hw_spi_obj_t;

extern const mp_obj_type_t          machine_hw_spi_type;
extern const machine_hw_spi_obj_t * machine_hw_spi_objs[];

const machine_hw_spi_obj_t        * hw_spi_find(mp_obj_t user_obj);

#endif /* __MACHINE_SPI_H__ */

