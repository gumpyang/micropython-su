/* machine_pwm.h */

#ifndef __MACHINE_PWM_H__
#define __MACHINE_PWM_H__

#include "py/runtime.h"

#include "machine_pin.h"
#include "hal_tim.h"

#define MACHIEN_PWM_CH_NUM_PER_TIM  4u
#define MACHINE_PWM_PORT_NUM        6u
#define MACHIEN_PWM_CH_NUM_ALL     (MACHIEN_PWM_CH_NUM_PER_TIM * MACHINE_PWM_PORT_NUM)
//#define MACHIEN_PWM_CH_NUM_ALL     (6u)
#define MACHINE_PWM_DUTY_NUM       1000u

/* multiple channels share the same tim counter. */
typedef struct
{
    uint32_t active_channels; /* mask. */
    uint32_t freq; /* pwm freq, the step_freq_hz = freq_hz / MACHINE_PWM_DUTY_NUM. */
} machine_pwm_port_conf_t;

/* for each channel. */
typedef struct
{
    uint32_t duty; /* max to MACHINE_PWM_DUTY_NUM. */
} machine_pwm_ch_conf_t;

/* PWM class instance configuration structure. */
typedef struct
{
    mp_obj_base_t base;      // object base class.

    const machine_pin_obj_t * pwm_pin_obj;
    uint32_t pwm_pin_af;

    TIM_Type * tim_port;
    machine_pwm_port_conf_t *port_conf;

    uint32_t tim_ch;
    machine_pwm_ch_conf_t *ch_conf;

    uint32_t pwm_id;
} machine_pwm_obj_t;

extern const machine_pwm_obj_t * machine_pwm_objs[];
extern const mp_obj_type_t       machine_pwm_type;

const machine_pwm_obj_t *pwm_find(mp_obj_t user_obj);



#endif /* __MACHINE_PIN_H__ */

