/* hal_gpio.h */

#ifndef __HAL_GPIO_H__
#define __HAL_GPIO_H__

#include "hal_common.h"

#define GPIO_PIN_0    (1u << 0u  )
#define GPIO_PIN_1    (1u << 1u  )
#define GPIO_PIN_2    (1u << 2u  )
#define GPIO_PIN_3    (1u << 3u  )
#define GPIO_PIN_4    (1u << 4u  )
#define GPIO_PIN_5    (1u << 5u  )
#define GPIO_PIN_6    (1u << 6u  )
#define GPIO_PIN_7    (1u << 7u  )
#define GPIO_PIN_8    (1u << 8u  )
#define GPIO_PIN_9    (1u << 9u  )
#define GPIO_PIN_10   (1u << 10u )
#define GPIO_PIN_11   (1u << 11u )
#define GPIO_PIN_12   (1u << 12u )
#define GPIO_PIN_13   (1u << 13u )
#define GPIO_PIN_14   (1u << 14u )
#define GPIO_PIN_15   (1u << 15u )
#define GPIO_PIN_16   (1u << 16u )
#define GPIO_PIN_17   (1u << 17u )
#define GPIO_PIN_18   (1u << 18u )
#define GPIO_PIN_19   (1u << 19u )
#define GPIO_PIN_20   (1u << 20u )
#define GPIO_PIN_21   (1u << 21u )
#define GPIO_PIN_22   (1u << 22u )
#define GPIO_PIN_23   (1u << 23u )
#define GPIO_PIN_24   (1u << 24u )
#define GPIO_PIN_25   (1u << 25u )
#define GPIO_PIN_26   (1u << 26u )
#define GPIO_PIN_27   (1u << 27u )
#define GPIO_PIN_28   (1u << 28u )
#define GPIO_PIN_29   (1u << 29u )
#define GPIO_PIN_30   (1u << 30u )
#define GPIO_PIN_31   (1u << 31u )


typedef enum
{
    GPIO_PinMux_GPIO,
    GPIO_PinMux_AF_1,
    GPIO_PinMux_AF_2,
} GPIO_PinMux_Type;

typedef enum
{
    GPIO_PinMode_HighImpedance,
    GPIO_PinMode_PullupWeak,    /* 1M ohm.  */
    GPIO_PinMode_PullUp,        /* 25K ohm. */
    GPIO_PinMode_PullDownWeak,  /* 1M ohm.  */
    GPIO_PinMode_PullDown,      /* 25K ohm. */
    GPIO_PinMode_Out_DriverStrength_0,
    GPIO_PinMode_Out_DriverStrength_1,
    GPIO_PinMode_Out_DriverStrength_2,
    GPIO_PinMode_Out_DriverStrength_3,
} GPIO_PinMode_Type;

typedef enum
{
    GPIO_InterruptMode_LevelLow,
    GPIO_InterruptMode_LevelHigh,
    GPIO_InterruptMode_EdgeFalling,
    GPIO_InterruptMode_EdgeRising,
    GPIO_InterruptMode_EdgeBoth,
} GPIO_InterruptMode_Type;

void     GPIO_SetPinMux(GPIO_Type * GPIOx, uint32_t pins, GPIO_PinMux_Type mux);

bool     GPIO_ReadInDataBit(GPIO_Type * GPIOx, uint32_t pins);
uint32_t GPIO_ReadInData(GPIO_Type * GPIOx);
void     GPIO_WriteBit(GPIO_Type * GPIOx, uint32_t pins, uint32_t val);
void     GPIO_WriteBits(GPIO_Type * GPIOx, uint32_t val);
void     GPIO_SetPinMode(GPIO_Type * GPIOx, uint32_t pins, GPIO_PinMode_Type mode, bool af_volt);
void     GPIO_SetDirection(GPIO_Type * GPIOx, uint32_t pins, bool output);

uint32_t GPIO_GetInterruptStatus(GPIO_Type * GPIOx);
void     GPIO_ClearInterruptStatus(GPIO_Type * GPIOx, uint32_t flags);
void     GPIO_EnableInterrupts(GPIO_Type * GPIOx, uint32_t flags, bool enable);
uint32_t GPIO_GetEnabledInterrupts(GPIO_Type * GPIOx);
void     GPIO_SetInterruptEvent(GPIO_Type * GPIOx, uint32_t pins, GPIO_InterruptMode_Type mode);

#endif /* __HAL_GPIO_H__ */

