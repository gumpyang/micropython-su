/* hal_icc.c */
#include "hal_icc.h"

uint32_t ICC_GetIpVerInfo(ICC_Type * ICCx)
{
    return ICCx->INFO;
}

void ICC_EnableCache(ICC_Type * ICCx)
{
    /* invalidate cache and wait until ready */
    ICCx->CTRL &= ~ICC_CTRL_EN_MASK;
    ICCx->INVALIDATE = 1u;
    while (0u == (ICCx->CTRL & ICC_CTRL_RDY_MASK))
    {}

    /* enable the cache. */
    ICCx->CTRL |= ICC_CTRL_EN_MASK;
    while (0u == (ICCx->CTRL & ICC_CTRL_RDY_MASK))
    {}
}

void ICC_DisableCache(ICC_Type * ICCx)
{
    ICCx->CTRL &= ~ICC_CTRL_EN_MASK;
}

/* EOF. */

