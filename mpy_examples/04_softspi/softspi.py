from machine import SoftSPI
from machine import Pin

spiflash_spi= SoftSPI(baudrate=10000, sck='PB10', mosi='PB15', miso='PB14')
spiflash_cs = Pin('PE3', mode=Pin.OUT_PUSHPULL, value=1)

spiflash_cs.low()
spiflash_spi.write(chr(int('9f',16)))
spiid = spiflash_spi.read(3)
spiflash_cs.high()



#spiflash_spi.write(chr(int('9f',16)))

