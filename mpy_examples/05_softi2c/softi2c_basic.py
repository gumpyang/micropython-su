from machine import SoftI2C
from machine import Pin

scl = Pin('PC6')
sda = Pin('PC7')
i2c = SoftI2C(scl, sda, freq=10000)
devaddr = i2c.scan()
print(devaddr)

buf_read  = i2c.readfrom_mem(devaddr[0], 8, 4)
print(buf_read)

buf_write = bytearray(4)
for i in range(4):
    buf_write[i] = (buf_read[i] + 1) % 256

i2c.writeto_mem(devaddr[0], 8, buf_write)
buf_read = i2c.readfrom_mem(devaddr[0], 8, 4)
print(buf_read)
print(hex(int.from_bytes(buf_read, 'big')))

