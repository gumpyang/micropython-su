import utime
from machine import Pin

# led0 = Pin('PA15', mode = Pin.OUT_PUSHPULL)
led0 = Pin('PA0', mode = Pin.OUT_PUSHPULL)
led1 = Pin('PG13', mode = Pin.OUT_PUSHPULL)

while True:
    led0.low()
    led1.high()
    utime.sleep_ms(200)
    led0.high()
    led1.low()
    utime.sleep_ms(200)
