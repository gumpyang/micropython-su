from machine import Pin
import time

led0 = Pin('PC4', mode=Pin.OUT_PUSHPULL, value=1)
led1 = Pin('PC5', mode=Pin.OUT_PUSHPULL, value=0)

while True:
	led0(1-led0())
	time.sleep_ms(200)
	led1(1-led1())
	time.sleep_ms(200)
